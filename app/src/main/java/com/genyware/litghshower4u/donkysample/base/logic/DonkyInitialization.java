package com.genyware.litghshower4u.donkysample.base.logic;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.genyware.litghshower4u.donkysample.utils.Constants;

import net.donky.core.DonkyCore;
import net.donky.core.DonkyException;
import net.donky.core.DonkyListener;
import net.donky.core.ModuleDefinition;
import net.donky.core.NotificationListener;
import net.donky.core.Subscription;
import net.donky.core.account.UserDetails;
import net.donky.core.network.DonkyNetworkController;
import net.donky.core.network.ServerNotification;
import net.donky.core.network.content.ContentNotification;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by litghshower4u on 17.07.2015.
 */
public class DonkyInitialization {

    public static String TAG = DonkyInitialization.class.getSimpleName();

    public static String MY_CUSTOM_NOTIFICATION = "MY_CUSTOM_NOTIFICATION";
    public static String MESSAGE_KEY = "MESSAGE_KEY";

    public static String USER_NAME = "jhond3dfq23redfsed";

    public static void init(Application checkAuthority) {
        if (checkAuthority == null) {
            throw new IllegalStateException("Could be NULL. Call it from Application.onCreate();");
        }

        UserDetails userDetails = new UserDetails();

        // setting the user id is the minimum for known user registration,
        // but there are many other informatins you can tell Donky Network about the registering user.
        userDetails.setUserId(USER_NAME).setUserDisplayName("JhennyZhmenny");

        /**
         * bellow code aim is:
         register your device in Google Cloud Messaging
         register new user on Donky Network
         create Device entity on Donky Network
         prepare you application to send and receive messages to Donky Network or another devices registered in the same AppSpace.
         */
        DonkyCore.initialiseDonkySDK(checkAuthority,
                Constants.API_KEY,
                userDetails,
                null,
                "1.0.0.0",
                new DonkyListener() {
                    @Override
                    public void success() {
                        Log.e(TAG, "Donky SDK initialized");// change to Log.i();
                    }

                    @Override
                    public void error(DonkyException e, Map<String, String> map) {
                        String exc = e.getMessage() == null ? "null" : e.getMessage();
                        Log.e(TAG, "Donky exception: " + exc);
                        if (map != null)
                            for (String item : map.values()) {
                                Log.e(TAG, item);
                            }
                    }
                });






    }

    public static void subscibeToNotifications(Subscription<ServerNotification> subscription) {
        DonkyCore.subscribeToContentNotifications(
                new ModuleDefinition("DonkySampleApp5", "1.0.0.0"), subscription);
    }

    static int TEMP_NUMBER_FOR_CUSTOM_NOTIFICATION;

    public static void sendMessage(final Context context) throws JSONException {
        //Create JSONObject containing your message.
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(MESSAGE_KEY, "hello world" + TEMP_NUMBER_FOR_CUSTOM_NOTIFICATION++);

        //Content notification of type 'chatMessage' to be send to user 'johnsmith'
        ContentNotification notification =
                new ContentNotification(USER_NAME, MY_CUSTOM_NOTIFICATION, jsonObject);

        //Send content
        DonkyNetworkController.getInstance().sendContentNotification(notification,
                new DonkyListener() {
                    @Override
                    public void success() {
                        Log.e(TAG, "Message successfully send");// change to Log.i();
                        Toast.makeText(context,"Message successfully send",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void error(DonkyException e, Map<String, String> map) {
                        Toast.makeText(context,"Message send has failed",Toast.LENGTH_SHORT).show();
                        String exc = e.getMessage() == null ? "null" : e.getMessage();
                        Log.e(TAG, "Donky send message exception: " + exc);
                        if (map != null)
                            for (String item : map.values()) {
                                Log.e(TAG, item);
                            }
                    }
                });
    }

}
