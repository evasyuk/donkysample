package com.genyware.litghshower4u.donkysample.base.UI;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.genyware.litghshower4u.donkysample.R;
import com.genyware.litghshower4u.donkysample.base.model.PushMessageModel;

import java.util.List;

/**
 * Created by litghshower4u on 16.07.2015.
 */
public class UI_level {

    static final int MAIN_LAYOUT_ID = R.layout.custom_layout;
    static final int LIST_ITEM_LAYOUT = R.layout.message_item_layout;

    static final int TEXT_VIEW_HOW_MANY_PUSHES_RECEIVED = R.id.textView;
    static final int TEXT_VIEW_PUSH_MESSAGE_ITSELF_RECEIVED = R.id.textView2;
    static final int LIST_VIEW_CONTAINER = R.id.my_super_container;
    static final int ADD = R.id.button;

    static TextView howManyPushesReceived_TV;

    static LinearLayout viewContainer;

    static LayoutInflater inflater;
    static LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    static Button addButton;


    public void setContentView(Activity activity) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        activity.setContentView(MAIN_LAYOUT_ID);

        howManyPushesReceived_TV = (TextView)activity.findViewById(TEXT_VIEW_HOW_MANY_PUSHES_RECEIVED);
        addButton = (Button) activity.findViewById(ADD);

        viewContainer = (LinearLayout)activity.findViewById(LIST_VIEW_CONTAINER);
    }

    public void setOnAddClickListener(View.OnClickListener listener) {
        addButton.setOnClickListener(listener);
    }

    public void addItemsToContainer(List<PushMessageModel> messages) {
        if (inflater == null) {
            throw new IllegalStateException(UI_level.class.getSimpleName()
                    + " LayoutInflater is not initialized. Call setContentView(int) before any further actions");
        }

        if (viewContainer == null) {
            throw new IllegalStateException(UI_level.class.getSimpleName()
                    + " viewContainer is not initialized. Call setContentView(int) before any further actions");
        }

        for (int index = 0; index < messages.size(); index++) {
            addItemToContainer(messages.get(index));
        }

        setPushNumberInStack();
    }

    public void addItemToContainer(PushMessageModel item) {
        if (item.getMessage() == null) { // redundant
            throw new IllegalStateException("Message object couldn't be NULL");
        }

        RelativeLayout listViewRoot = (RelativeLayout) inflater.inflate(LIST_ITEM_LAYOUT, viewContainer, false);

        TextView pushMessageItself_TV  = (TextView)listViewRoot.findViewById(TEXT_VIEW_PUSH_MESSAGE_ITSELF_RECEIVED);
        pushMessageItself_TV.setText(item.getMessage());

        viewContainer.addView(listViewRoot, layoutParams);
    }

    private void setPushNumberInStack() {
        if (howManyPushesReceived_TV == null) {
            throw new IllegalStateException(UI_level.class.getSimpleName()
                    + " howManyPushesReceived_TV is not initialized. Call setContentView(int) before any further actions");
        }

        howManyPushesReceived_TV.setText("" + viewContainer.getChildCount());
    }

    public void clearContainer() {
        for (int index = 0; index < viewContainer.getChildCount(); index++) {
            viewContainer.removeView(viewContainer.getChildAt(index)); // I use this approach because of unexpected behaviour View.removeViews(..)
        }
    }

}
