package com.genyware.litghshower4u.donkysample.base.UI;

import com.genyware.litghshower4u.donkysample.base.model.PushMessageModel;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by litghshower4u on 16.07.2015.
 */
public class TestUIData {

    public static List<PushMessageModel> getCollection() {
        List<PushMessageModel> result = new LinkedList<>();

        for (int index = 0; index < 10; index++) {
            PushMessageModel item = new PushMessageModel();
            item.setMessage("item" + index);

            result.add(item);
        }

        return result;
    }



}
