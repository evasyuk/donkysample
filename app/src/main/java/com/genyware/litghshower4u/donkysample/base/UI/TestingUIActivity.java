package com.genyware.litghshower4u.donkysample.base.UI;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.genyware.litghshower4u.donkysample.base.model.PushMessageModel;
import com.genyware.litghshower4u.donkysample.base.logic.DonkyInitialization;

import net.donky.core.NotificationListener;
import net.donky.core.Subscription;
import net.donky.core.network.ServerNotification;

import org.json.JSONException;

/**
 * Created by litghshower4u on 16.07.2015.
 */
public class TestingUIActivity extends AppCompatActivity {

    public static final String TAG = TestingUIActivity.class.getSimpleName();

    public static final int SEND_MESSAGE = 0;
    public static final int REMOVE_ALL = 1;

    Context tempContext;

    UI_level ui = new UI_level();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tempContext = this;

        ui.setContentView(this);

        // debug purpose
        //ui.addItemsToContainer(TestUIData.getCollection());

        View.OnClickListener onAddClikcListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "SEND_MESSAGE");

                try {
                    DonkyInitialization.sendMessage(tempContext);
                } catch (JSONException e) {
                    Log.e(TAG, "JSONException " + (e.getMessage() == null ? " NULL" : e.getMessage()));
                    Toast.makeText(tempContext, "JSONException", Toast.LENGTH_SHORT).show();
                }
            }
        };
        ui.setOnAddClickListener(onAddClikcListener);

        Subscription<ServerNotification> subscription = new Subscription<>(DonkyInitialization.MY_CUSTOM_NOTIFICATION,
                new NotificationListener<ServerNotification>() {

                    @Override
                    public void onNotification(ServerNotification notification) {
                        /* Code to handle the notification(s) as they are received and
                        processed */
                        String result = notification.getData().get("customData").getAsJsonObject().get(DonkyInitialization.MESSAGE_KEY).getAsString();
         
                        if (result != null) {
                            PushMessageModel model = new PushMessageModel();
                            model.setMessage(result);

                            ui.addItemToContainer(model);
                        } else {
                            Toast.makeText(tempContext, "result == null, but hasn't to",Toast.LENGTH_SHORT).show();
                            Log.e(TAG, "result == null");
                        }
                    }

                });
        DonkyInitialization.subscibeToNotifications(subscription);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        menu.add(0, SEND_MESSAGE, 0, "SEND_MESSAGE");
        menu.add(0,REMOVE_ALL,1,"REMOVE_ALL");

        return true;
    }
    public boolean onOptionsItemSelected (MenuItem item)
    {
        switch(item.getItemId())
        {
            case(SEND_MESSAGE):
                Log.d(TAG, "SEND_MESSAGE");

                try {
                    DonkyInitialization.sendMessage(tempContext);
                } catch (JSONException e) {
                    Log.e(TAG, "JSONException " + (e.getMessage() == null ? " NULL" : e.getMessage()));
                    Toast.makeText(tempContext, "JSONException", Toast.LENGTH_SHORT).show();
                }
                break;
            case(REMOVE_ALL):
                Log.d(TAG, "REMOVE_ALL");
                ui.clearContainer();
                break;
        }
        return false;
    }
}
