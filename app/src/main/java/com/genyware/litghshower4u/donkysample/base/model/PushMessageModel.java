package com.genyware.litghshower4u.donkysample.base.model;

/**
 * Created by litghshower4u on 16.07.2015.
 */
public class PushMessageModel {

    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
