package com.genyware.litghshower4u.donkysample.base;

import android.app.Application;
import android.util.Log;

import com.genyware.litghshower4u.donkysample.base.logic.DonkyInitialization;
import com.genyware.litghshower4u.donkysample.utils.Constants;

import net.donky.core.DonkyCore;
import net.donky.core.DonkyException;
import net.donky.core.DonkyListener;
import net.donky.core.ModuleDefinition;
import net.donky.core.NotificationListener;
import net.donky.core.Subscription;
import net.donky.core.account.DeviceDetails;
import net.donky.core.account.UserDetails;
import net.donky.core.automation.DonkyAutomation;
import net.donky.core.messaging.push.logic.DonkyPushLogic;
import net.donky.core.network.ServerNotification;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by litghshower4u on 14.07.2015.
 */
public class SampleApplication extends Application {

    public static final String TAG = SampleApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        DonkyInitialization.init(this);
    }

}