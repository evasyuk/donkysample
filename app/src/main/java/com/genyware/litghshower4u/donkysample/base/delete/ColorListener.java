package com.genyware.litghshower4u.donkysample.base.delete;

/**
 * Created by marcin.swierczek on 13/03/2015.
 */
public interface ColorListener {

    public void onColorChanged(String color, int interval);

}